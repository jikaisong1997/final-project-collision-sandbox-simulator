How to run this program:
Run ‘make’ from the command line in this path.
How to play:
The user changes the sandbox between the three different modes by press "1", "2", "3" of the keyboard.
1. Mode 1
If Mode 1 is selected (by pressing the '1' key), the sandbox shows the collision simulation of two objects. The 'p' key pauses playback and restarts it. The '1' key returns the balls to their home positions.
Pressing the 'c' key displays the 1st particular case of a central collision. Pressing the 'c' key again displays the next case, and so on.
The '0' key puts the balls in position and assigns them speed and direction so that they collide with each other.
1.1 Changing ball attributes
It is possible to change the mass, radius, and speed in all three directions and position it with the balls.
First, you need to press the 'a' key to start changing the attributes. After you can select:
'R' for radius'm' for mass
‘P’ then ‘x’ / ‘y’ / ‘z’ for position ‘v’ then ‘x’ / ‘y’ / ‘z’ for speed
'B' to change the ball whose attributes we change
After selecting the attribute, use the '+' and '-' keys to change the value of the selected attribute. When the desired value is reached, we can select another attribute. When you have finished changing the attributes, press 'q.'
2 Mode 2
Pressing the '2' key displays the 2nd mode. And this mode is run by the Brute Force method. You can see there are balls in a box. Pressing the 'w' key changes the option of drawing walls. Pressing the space bar, there Will randomly add 20 balls to the box.
3 Mode 3
Pressing the '3' key displays the 3rd mode. And this mode is run by the Octree method. You can see there are balls in a box. Pressing the 'w' key changes the option of drawing walls. By pressing the space bar we add 20 balls. Use the 'o' key to change the option to draw the octal tree.
